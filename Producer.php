<?php

class Producer
{
    protected $producer;

    protected $topics;

    public function __construct(array $topics)
    {
        $conf = new RdKafka\Conf();
        $conf->set('log_level', LOG_DEBUG);
        $conf->set('debug', 'all');
        $this->producer = new RdKafka\Producer($conf);
        $this->producer->setLogLevel(LOG_DEBUG);
        $this->producer->addBrokers('b-1.test-kafka.e6o2de.c3.kafka.us-east-1.amazonaws.com:9094,b-2.test-kafka.e6o2de.c3.kafka.us-east-1.amazonaws.com:9094');
        echo "Created producer\n";
        foreach ($topics as $topic) {
            echo "Created topic: $topic\n";
            $this->topics[] = $this->producer->newTopic($topic);
        }
    }

    public function handle()
    {
        echo "Start sending messages\n";
        for ($i = 0; $i < PHP_INT_MAX; $i++) {
            usleep(rand(1000000, 1000000));
            $topic = $this->topics[array_rand($this->topics)];
            echo "Sending message #$i to topic: {$topic->getName()}\n";
            var_dump($topic->produce(
                RD_KAFKA_PARTITION_UA,
                0,
                'Topic: ' . $topic->getName() . ';Message#:' . $i
            ));;
        }
    }
}