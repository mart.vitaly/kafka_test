<?php

class Consumer
{
    private $consumer;

    public function __construct(array $topics, string $group)
    {
        $conf = new RdKafka\Conf();
        // Set a rebalance callback to log partition assignments (optional)
        $conf->setRebalanceCb(function (RdKafka\KafkaConsumer $kafka, $err, array $partitions = null) {
            echo "Rebalancing kafka partitions\n";
            switch ($err) {
                case RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS:
                    echo "Assigning to patritions: \n";
                    foreach ($partitions as $partition) {
                        echo "- $partition: \n";
                    }
                    $kafka->assign($partitions);
                    break;

                case RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS:
                    echo "Revoke: ";
                    var_dump($partitions);
                    $kafka->assign(NULL);
                    break;

                default:
                    throw new \Exception($err);
            }
        });
        // Configure the group.id. All consumer with the same group.id will consume
        // different partitions.
        $conf->set('group.id', $group);

        // Initial list of Kafka brokers
        $conf->set('metadata.broker.list', 'b-1.test-kafka.e6o2de.c3.kafka.us-east-1.amazonaws.com:9094,b-2.test-kafka.e6o2de.c3.kafka.us-east-1.amazonaws.com:9094');

        // Set where to start consuming messages when there is no initial offset in
        // offset store or the desired offset is out of range.
        // 'smallest': start from the beginning
        $conf->set('auto.offset.reset', 'smallest');

        $this->consumer = new RdKafka\KafkaConsumer($conf);
        var_dump($this->consumer);
        // Subscribe to topics
        $this->consumer->subscribe($topics);

        echo "Waiting for partition assignment... (make take some time when\n";
        echo "quickly re-joining the group after leaving it.)\n";
    }

    public function handle()
    {
        while (true) {
            echo "quickly re-joining the group after leaving it.)\n";
            $message = $this->consumer->consume(120*1000);
            switch ($message->err) {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    var_dump($message);
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                    echo "No more messages; will wait for more\n";
                    break;
                case RD_KAFKA_RESP_ERR__TIMED_OUT:
                    echo "Timed out\n";
                    break;
                default:
                    throw new \Exception($message->errstr(), $message->err);
                    break;
            }
        }
    }
}