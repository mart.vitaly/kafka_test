<?php

include 'Producer.php';

$arguments = getopt('',['topics:']);
$topics = isset($arguments['topics'])
    ? explode(',', $arguments['topics'])
    : [];

(new Producer($topics))->handle();

