<?php

include 'Consumer.php';

$arguments = getopt('',['topics:']);
$topics = isset($arguments['topics'])
    ? explode(',', $arguments['topics'])
    : [];
$group = $arguments['group'] ?? 'default';

(new Consumer($topics, $group))->handle();
